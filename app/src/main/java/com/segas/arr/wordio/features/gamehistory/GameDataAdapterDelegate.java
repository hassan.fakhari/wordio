package com.segas.arr.wordio.features.gamehistory;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.segas.arr.wordio.R;
import com.segas.arr.wordio.commons.DurationFormatter;
import com.segas.arr.wordio.model.GameDataInfo;
import com.segas.arr.wordio.easyadapter.AdapterDelegate;



public class GameDataAdapterDelegate extends AdapterDelegate<GameDataInfo, GameDataAdapterDelegate.ViewHolder> {

    private OnClickListener mListener;

    public GameDataAdapterDelegate() {
        super(GameDataInfo.class);
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game_data_history, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GameDataInfo model, ViewHolder holder) {
        holder.textName.setText(model.getName());
        holder.textDuration.setText(DurationFormatter.fromInteger(model.getDuration()));
        String desc = holder.itemView.getContext().getString(R.string.game_data_desc);
        desc = desc.replaceAll(":gridSize", model.getGridRowCount() + "x" + model.getGridColCount());
        desc = desc.replaceAll(":wordCount", String.valueOf(model.getUsedWordsCount()));
        holder.textOtherDesc.setText(desc);

        holder.itemView.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onClick(model);
            }
        });
        holder.viewDeleteItem.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onDeleteClick(model);
            }
        });
    }

    public void setOnClickListener(OnClickListener listener) {
        mListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textName;
         TextView textDuration;
        TextView textOtherDesc;

        View viewDeleteItem;
        ViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.text_name);
            textDuration = itemView.findViewById(R.id.text_duration);
            textOtherDesc = itemView.findViewById(R.id.text_desc);
            viewDeleteItem = itemView.findViewById(R.id.delete_list_item);



        }
    }

    public interface OnClickListener {
        void onClick(GameDataInfo gameDataInfo);
        void onDeleteClick(GameDataInfo gameDataInfo);
    }
}
