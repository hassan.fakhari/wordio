package com.segas.arr.wordio.model.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

public class GetScoreByDeviceIdResponse {

    @Expose
    @SerializedName("message")
    private  String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Nullable
    @Expose
    @SerializedName("score")
    private String score;


}
