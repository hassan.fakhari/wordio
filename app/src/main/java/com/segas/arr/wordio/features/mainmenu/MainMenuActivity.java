package com.segas.arr.wordio.features.mainmenu;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.segas.arr.wordio.R;
import com.segas.arr.wordio.api.ServiceGenerator;
import com.segas.arr.wordio.api.service.WordService;
import com.segas.arr.wordio.data.sqlite.DbHelper;
import com.segas.arr.wordio.features.ViewModelFactory;
import com.segas.arr.wordio.Wordio;
import com.segas.arr.wordio.features.FullscreenActivity;
import com.segas.arr.wordio.features.gamehistory.GameHistoryActivity;
import com.segas.arr.wordio.features.gameplay.GamePlayActivity;
import com.segas.arr.wordio.model.GameTheme;
import com.segas.arr.wordio.easyadapter.MultiTypeAdapter;
import com.segas.arr.wordio.features.settings.SettingsActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.segas.arr.wordio.utils.SelectLevelDialog;
import com.segas.arr.wordio.utils.SharedPrefrance;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import static com.google.android.gms.games.Games.getGamesClient;


public class MainMenuActivity extends FullscreenActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

      RecyclerView mRv;
 //    Spinner mGridSizeSpinner;
TextView gameState;
Handler mainHandler;
    int[] mGameRoundDimVals;
SignInButton sign_in_button;
    @Inject
    ViewModelFactory mViewModelFactory;
    MainMenuViewModel mViewModel;
    MultiTypeAdapter mAdapter;

    Animation scale;

    // Client used to sign in with Google APIs
    private GoogleSignInClient mGoogleSignInClient = null;
    GoogleSignInAccount account;
    Button new_game_btn;
    GoogleApiClient apiClient;

public  void SelectLevel(View view){
    FragmentManager fm = getSupportFragmentManager();
    SelectLevelDialog selectLevelDialog = new SelectLevelDialog();
    selectLevelDialog.show(fm,"");


}


    public void showLeaderboard(View v) {
        showLeaderboard();
    }



    private static final int RC_LEADERBOARD_UI = 9004;

    private void showLeaderboard() {
        try {
            Games.getLeaderboardsClient(this, account)
                    .getLeaderboardIntent(getString(R.string.leaderboard_id))
                    .addOnSuccessListener(new OnSuccessListener<Intent>() {
                        @Override
                        public void onSuccess(Intent intent) {
                            startActivityForResult(intent, RC_LEADERBOARD_UI);
                        }
                    });
        }catch (Exception e){
            Toast.makeText(this,"Google Play Not Ready!",Toast.LENGTH_LONG).show();
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
  }
WordService api ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
api = ServiceGenerator.createService(WordService.class);
        gameState = findViewById(R.id.gameState);
        sign_in_button = findViewById(R.id.sign_in_button);




        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        com.google.android.gms.ads.AdView  mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
           mAdView.loadAd(adRequest);

        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Games.SCOPE_GAMES_LITE)

                .build();

GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this,gso);


account = GoogleSignIn.getLastSignedInAccount(this);

if(account==null){

}else{
    sign_in_button.setVisibility(View.GONE);
}


        sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent,1002);
            }
        });




/*

        apiClient = new GoogleApiClient.Builder(this, new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {

            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.w("www2", "onConnectionFailed: " + i);
            }
        }, new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                Log.w("www1", "onConnectionFailed: " + connectionResult.getErrorMessage() + " " + connectionResult .getErrorCode());
            }
        }).addApi(Games.API).build();
      apiClient.connect();
      */
       // signInSilently();
        try {
            new DbHelper(this).createDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainHandler = new Handler(Looper.getMainLooper());
        new_game_btn = findViewById(R.id.new_game_btn);


    mGameRoundDimVals = getResources().getIntArray(R.array.game_round_dimension_values);


        ((Wordio) getApplication()).getAppComponent().inject(this);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MainMenuViewModel.class);
        mViewModel.getOnGameThemeLoaded().observe((LifecycleOwner) this, this::showGameThemeList);

     switch (SharedPrefrance.GetLevel()){

         case 0:

             gameState.setText("Easy");
             break;

         case 1:
             gameState.setText("Normal");

             break;
         case 2:
             gameState.setText("Medium");
             break;


         case 3:


             gameState.setText("Pro");
             break;


     }



  // getGameHelper().enableDebugLog(true);
 //getGameHelper().beginUserInitiatedSignIn();

 //client.connect();

       // startSignInIntent();






        mAdapter = new MultiTypeAdapter();
        mAdapter.addDelegate(
                GameTheme.class,
                R.layout.item_game_theme,
                (model, holder) -> holder.<TextView>find(R.id.textThemeName).setText(model.getName()),
                (model, view) -> Toast.makeText(MainMenuActivity.this, model.getName(), Toast.LENGTH_SHORT).show()
        );
        mAdapter.addDelegate(
                HistoryItem.class,
                R.layout.item_histories,
                (model, holder) -> {},
                (model, view) -> {
                    Intent i = new Intent(MainMenuActivity.this, GameHistoryActivity.class);
                    startActivity(i);
                }
        );


        mViewModel.loadData();
    }

    public void showGameThemeList(List<GameTheme> gameThemes) {
        mAdapter.setItems(gameThemes);
        mAdapter.insertAt(0, new HistoryItem());
    }


    public void onSettingsClick(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void onNewGameClick(View view) {
        int dim = mGameRoundDimVals[SharedPrefrance.GetLevel()];
        Intent intent = new Intent(MainMenuActivity.this, GamePlayActivity.class);
        intent.putExtra(GamePlayActivity.EXTRA_ROW_COUNT, dim);
        intent.putExtra(GamePlayActivity.EXTRA_COL_COUNT, dim);
        startActivity(intent);
    }

    private void startSignInIntent() {
        GoogleSignInClient signInClient = GoogleSignIn.getClient(this,
                GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
        Intent intent = signInClient.getSignInIntent();
        startActivityForResult(intent, 1002);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1002) {
Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
               account = task.getResult(ApiException.class);
            } catch (ApiException e) {

                Log.w("www", "onActivityResult: " + e.getStatusCode() + " " + e.getMessage() + " " + e.getStatusMessage() );


            }



        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }






    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        try {
            connectionResult.startResolutionForResult(this,1002);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }



    }
}
