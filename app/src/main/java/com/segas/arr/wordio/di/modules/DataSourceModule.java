package com.segas.arr.wordio.di.modules;

import android.content.Context;

 import com.segas.arr.wordio.data.sqlite.DbHelper;
import com.segas.arr.wordio.data.sqlite.GameDataSQLiteDataSource;
import com.segas.arr.wordio.data.sqlite.WordSQLiteDataSource;
import com.segas.arr.wordio.data.GameDataSource;
import com.segas.arr.wordio.data.WordDataSource;
import com.segas.arr.wordio.data.xml.WordXmlDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;



@Module
public class DataSourceModule {

    @Provides
    @Singleton
    DbHelper provideDbHelper(Context context) {
        return new DbHelper(context);
    }

    @Provides
    @Singleton
    GameDataSource provideGameRoundDataSource(DbHelper dbHelper) {
        return new GameDataSQLiteDataSource(dbHelper);
    }

    @Provides
    @Singleton
     WordDataSource provideWordDataSource(DbHelper dbHelper) {
         return new WordSQLiteDataSource(dbHelper);
     }









/*
    @Provides
    @Singleton
    WordDataSource provideWordDataSource(Context context) {
        return new WordXmlDataSource(context);
    }
*/

}
