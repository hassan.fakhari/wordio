package com.segas.arr.wordio;

import android.app.Application;

import com.segas.arr.wordio.di.component.AppComponent;
import com.segas.arr.wordio.di.component.DaggerAppComponent;
import com.segas.arr.wordio.di.modules.AppModule;
import com.segas.arr.wordio.utils.SharedPrefrance;



public class Wordio extends Application {

    AppComponent mAppComponent;
/*

//admob
ca-app-pub-2728042490894538~6109894070

//tapcell
nbkfjbgqfbctsmpdcjkrcdfherbqajslmmgiaqqlppjorqcbligfjdmgqiqglpciqppghr

//





 */
    @Override
    public void onCreate() {
        super.onCreate();
        new SharedPrefrance(this);
        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

}
