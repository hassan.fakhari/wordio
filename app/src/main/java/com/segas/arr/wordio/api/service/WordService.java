package com.segas.arr.wordio.api.service;

import com.segas.arr.wordio.model.json.AddScoreRequest;
import com.segas.arr.wordio.model.json.AddScoreResponse;
import com.segas.arr.wordio.model.json.ChangeBuyRequest;
import com.segas.arr.wordio.model.json.ChangeBuyResponse;
import com.segas.arr.wordio.model.json.GetScoreByDeviceIdRequest;
import com.segas.arr.wordio.model.json.GetScoreByDeviceIdResponse;
import com.segas.arr.wordio.model.json.LoginUserRequest;
import com.segas.arr.wordio.model.json.LoginUserResponse;
import com.segas.arr.wordio.model.json.RegisterUserRequest;
import com.segas.arr.wordio.model.json.RegisterUserResponse;
import com.segas.arr.wordio.model.json.getWordsByCountRequest;
import com.segas.arr.wordio.model.json.getWordsByCountResponse;
import com.segas.arr.wordio.model.json.getWordsByLevelRequest;
import com.segas.arr.wordio.model.json.getWordsByLevelResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface WordService {






    @POST("")
    Call<RegisterUserResponse> RegisterUser(@Body RegisterUserRequest param);

    @POST("")
    Call<LoginUserResponse> LoginUser(@Body LoginUserRequest param);

@POST("")
    Call<AddScoreResponse> AddScore(@Body AddScoreRequest param);


@POST("")
    Call<ChangeBuyResponse> ChangeBuy(@Body ChangeBuyRequest param);


@POST("")
    Call<GetScoreByDeviceIdResponse> GetScoreByDeviceId(@Body GetScoreByDeviceIdRequest param);

@POST
Call<getWordsByCountResponse> getWordsByCount(@Body getWordsByCountRequest param);


@POST("")
    Call<getWordsByLevelResponse> getWordsByLevel(@Body getWordsByLevelRequest param);












}
