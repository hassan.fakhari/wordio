package com.segas.arr.wordio.data.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.segas.arr.wordio.data.WordDataSource;
import com.segas.arr.wordio.model.Word;
import com.segas.arr.wordio.utils.SharedPrefrance;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class WordSQLiteDataSource implements WordDataSource {

    private DbHelper mHelper;

    @Inject
    public WordSQLiteDataSource(DbHelper helper) {
        mHelper = helper;
    }

    @Override
    public List<Word> getWords(int level) {
        SQLiteDatabase db = mHelper.getReadableDatabase();

        String cols[] = {
                DbContract.WordBank._ID,
                DbContract.WordBank.COL_STRING
        };
Cursor c = null;

if(SharedPrefrance.GetTable()==0) {
     c = db.rawQuery("SELECT * FROM  word WHERE level='" + level + "'", null);
}else{
    c = db.rawQuery("SELECT * FROM  wordfa WHERE level='" + level + "'", null);
}


        List<Word> wordList = new ArrayList<>();
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {

                wordList.add(new Word(c.getInt(0), c.getString(1).toLowerCase(),c.getInt(2)));

                c.moveToNext();
            }
        }

        c.close();
        return wordList;
    }

    @Override
    public void InsertWords() {

    }
}