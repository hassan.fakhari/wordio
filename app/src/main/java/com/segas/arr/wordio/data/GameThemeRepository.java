package com.segas.arr.wordio.data;

import com.segas.arr.wordio.model.GameTheme;

import java.util.ArrayList;
import java.util.List;

public class GameThemeRepository {

    public List<GameTheme> getGameThemes() {
        // sample data
        List<GameTheme> themes = new ArrayList<>();
        /*
        themes.add(new GameTheme(pica, "Fruit"));
        themes.add(new GameTheme(picb, "Vegetable"));
        themes.add(new GameTheme(picc, "Programming"));
        themes.add(new GameTheme(picd, "Food"));
        themes.add(new GameTheme(pice, "Animal"));
        themes.add(new GameTheme(pice, "Animal pica"));
        themes.add(new GameTheme(pice, "Animal picb"));
        themes.add(new GameTheme(pice, "Animal picc"));
        themes.add(new GameTheme(pice, "Animal picd"));
        themes.add(new GameTheme(pice, "Animal pice"));
        themes.add(new GameTheme(pice, "Animal 6"));
        */
        return themes;
    }

}
