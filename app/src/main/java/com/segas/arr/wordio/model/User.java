package com.segas.arr.wordio.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getUser_score() {
        return user_score;
    }

    public void setUser_score(String user_score) {
        this.user_score = user_score;
    }




        /*

{"message":"success","data":{"username":"hasan","phone_number":"0","device_id":"2676","user_score":"10","has_buy":"0"}}
     */


        @Expose
    @SerializedName("username")
    private String username;


        @Expose
    @SerializedName("phone_number")
    private  String phone_number;


        @Expose
    @SerializedName("device_id")
        private  String device_id;


        @Expose
    @SerializedName("user_score")
    private String user_score;




}
