package com.segas.arr.wordio.features.gamehistory;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.segas.arr.wordio.R;
import com.segas.arr.wordio.Wordio;
import com.segas.arr.wordio.easyadapter.MultiTypeAdapter;
import com.segas.arr.wordio.features.FullscreenActivity;
import com.segas.arr.wordio.features.ViewModelFactory;
import com.segas.arr.wordio.features.gameplay.GamePlayActivity;
import com.segas.arr.wordio.model.GameDataInfo;

import java.util.List;

import javax.inject.Inject;



public class GameHistoryActivity extends FullscreenActivity {


    TextView mTextEmpty;

    RecyclerView mRecyclerView;

    @Inject
    ViewModelFactory mViewModelFactory;
    private GameHistoryViewModel mViewModel;

    private MultiTypeAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_history);

        ((Wordio) getApplication()).getAppComponent().inject(this);

        mTextEmpty = findViewById(R.id.textEmpty);
        mRecyclerView = findViewById(R.id.recyclerView);

        initRecyclerView();

        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(GameHistoryViewModel.class);
        mViewModel.getOnGameDataInfoLoaded().observe(this, this::onGameDataInfoLoaded);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViewModel.loadGameHistory();
    }


    public void onButtonClearClick(View view) {
        mViewModel.clear();
    }

    private void onGameDataInfoLoaded(List<GameDataInfo> gameDataInfos) {
        if (gameDataInfos.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            mTextEmpty.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mTextEmpty.setVisibility(View.GONE);
            mAdapter.setItems(gameDataInfos);
        }
    }

    private void initRecyclerView() {
        GameDataAdapterDelegate gameDataAdapterDelegate = new GameDataAdapterDelegate();
        gameDataAdapterDelegate.setOnClickListener(new GameDataAdapterDelegate.OnClickListener() {
            @Override
            public void onClick(GameDataInfo gameDataInfo) {
                Intent intent = new Intent(GameHistoryActivity.this, GamePlayActivity.class);
                intent.putExtra(GamePlayActivity.EXTRA_GAME_ROUND_ID, gameDataInfo.getId());
                startActivity(intent);
            }

            @Override
            public void onDeleteClick(GameDataInfo gameDataInfo) {
                mViewModel.deleteGameData(gameDataInfo);
            }
        });

        mAdapter = new MultiTypeAdapter();
        mAdapter.addDelegate(gameDataAdapterDelegate);

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
    }
}
