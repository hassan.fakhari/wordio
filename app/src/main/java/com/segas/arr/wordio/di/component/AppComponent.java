package com.segas.arr.wordio.di.component;

import com.segas.arr.wordio.di.modules.AppModule;
import com.segas.arr.wordio.di.modules.DataSourceModule;
import com.segas.arr.wordio.features.FullscreenActivity;
import com.segas.arr.wordio.features.gamehistory.GameHistoryActivity;
import com.segas.arr.wordio.features.gameover.GameOverActivity;
import com.segas.arr.wordio.features.gameplay.GamePlayActivity;
import com.segas.arr.wordio.features.mainmenu.MainMenuActivity;

import javax.inject.Singleton;

import dagger.Component;



@Singleton
@Component(modules = {AppModule.class, DataSourceModule.class})
public interface AppComponent {

    void inject(GamePlayActivity activity);

    void inject(MainMenuActivity activity);

    void inject(GameOverActivity activity);

    void inject(FullscreenActivity activity);

    void inject(GameHistoryActivity activity);

}
