package com.segas.arr.wordio.model.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterUserResponse {


    //RegisterUser

    @Expose
    @SerializedName("message")
    private  String message;



    @Expose
    @SerializedName("device_id")
    private String device_id;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}
