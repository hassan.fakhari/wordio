package com.segas.arr.wordio.features.gameover;

import androidx.lifecycle.ViewModelProviders;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import androidx.core.app.NavUtils;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.games.Games;
import com.segas.arr.wordio.R;
import com.segas.arr.wordio.features.ViewModelFactory;
import com.segas.arr.wordio.Wordio;
import com.segas.arr.wordio.commons.DurationFormatter;
import com.segas.arr.wordio.model.GameDataInfo;
import com.segas.arr.wordio.features.FullscreenActivity;
import com.segas.arr.wordio.utils.ActionCenteral;
import com.segas.arr.wordio.utils.TextAnimationFields;

import javax.inject.Inject;



public class GameOverActivity extends FullscreenActivity {
    public static final String EXTRA_GAME_ROUND_ID =
            "com.paperplanes.wordsearch.presentation.ui.activity.GameOverActivity";

    @Inject
    ViewModelFactory mViewModelFactory;
    TextAnimationFields textAnimationFields;
    TextView[] myTextViews, myTextViewsOut;
    LinearLayout linearLayout;

    TextView mGameStatText,rowTextView,rowTextViewOut;
public RelativeLayout childLayout;
public LinearLayout.LayoutParams linearParams;


    private int mGameId;
    private GameOverViewModel mViewModel;



    public void setEditTextFields(String textFields) {
        String editedTextNumber = textFields;
        String vvv = "3000";
        String gapBetweenTwoNumbersDurationString = "1000";
        int[]   animatingNumbers;

        textAnimationFields.setGapBetweenTwoNumbersDuration(Integer.parseInt(gapBetweenTwoNumbersDurationString));
        textAnimationFields.setAnimationDuration(Integer.parseInt(vvv));
        if (textAnimationFields.getAnimationDuration() < 100) {
            textAnimationFields.setAnimationDuration(100);
            Toast.makeText(getApplicationContext(), "We have set 100 as minimum animation time!", Toast.LENGTH_LONG).show();
        }

        linearLayout.removeAllViews();
        if (editedTextNumber.length() != 0) {
            textAnimationFields.setMaxNumbers(editedTextNumber.length());
            char NArray[] = editedTextNumber.toCharArray();
            animatingNumbers = new int[7];
            for (int i = 0; i < editedTextNumber.length(); i++) {
                animatingNumbers[i] = Integer.parseInt("" + NArray[i]);
            }
        } else {
            animatingNumbers = new int[]{1, 6, 9, 5};

        }
    }





    public void createNumberOfTextFields() {

        myTextViews = new TextView[textAnimationFields.getMaxNumbers()]; // create an empty array;
        myTextViewsOut = new TextView[textAnimationFields.getMaxNumbers()]; // create an empty array;

        for (int i = 0; i < textAnimationFields.getMaxNumbers(); i++) {
            childLayout = new RelativeLayout(GameOverActivity.this);

            linearParams = new LinearLayout.LayoutParams((textAnimationFields.getTextSize() * 3),
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            childLayout.setBackgroundDrawable(getBaseContext().getResources().getDrawable(R.drawable.bg));
            linearParams.setMargins(30, 0, 30, 0);

            childLayout.setLayoutParams(linearParams);

            // create a new textview
            rowTextView = new TextView(this);
            rowTextViewOut = new TextView(this);

            rowTextView.setLayoutParams(new TableLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
            rowTextViewOut.setLayoutParams(new TableLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
            rowTextView.setGravity(Gravity.CENTER);
            rowTextViewOut.setGravity(Gravity.CENTER);

            childLayout.addView(rowTextViewOut, 0);
            childLayout.addView(rowTextView, 0);

            linearLayout.addView(childLayout);
            rowTextView.setTextSize((float) textAnimationFields.getTextSize());
            rowTextViewOut.setTextSize((float) textAnimationFields.getTextSize());
            rowTextView.setTextColor(Color.WHITE);
            rowTextViewOut.setTextColor(Color.WHITE);


                rowTextView.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                rowTextViewOut.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));


            myTextViews[i] = rowTextView;
            myTextViewsOut[i] = rowTextViewOut;

            animateTexts(1, 0, myTextViews[i], myTextViewsOut[i]);

        }
    }
    public void animateTexts(final int actualNo, final int loopNo, final TextView textView, final TextView textViewOut) {
        textViewOut.setText(" " + loopNo + " ");
        textView.setVisibility(View.GONE);


        if (actualNo == loopNo) {
            textViewOut.setText(" " + actualNo + " ");
            textView.setText(" " + actualNo + " ");
            textView.setVisibility(View.VISIBLE);
        } else {
            AnimatorSet animatorSet2 = new AnimatorSet();
            animatorSet2.setInterpolator(new LinearInterpolator());
            animatorSet2.playTogether(
                    ObjectAnimator.ofFloat(textViewOut, "translationY", 0, (textAnimationFields.getTextSize() * 3))
            );
            animatorSet2.setDuration(textAnimationFields.getAnimationDuration());
            animatorSet2.addListener(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                }
            });
            animatorSet2.start();


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    textView.setVisibility(View.VISIBLE);
                    AnimatorSet animatorSet2 = new AnimatorSet();
                    animatorSet2.playTogether(
                            ObjectAnimator.ofFloat(textView, "translationY", -(textAnimationFields.getTextSize() * 3), 0)
                    );
                    animatorSet2.setDuration(textAnimationFields.getAnimationDuration());
                    animatorSet2.setInterpolator(new LinearInterpolator());
                    animatorSet2.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            if (actualNo < loopNo)
                                textView.setText(" " + (loopNo - 1) + " ");
                            else
                                textView.setText(" " + (loopNo + 1) + " ");
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            if (actualNo < loopNo)
                                animateTexts(actualNo, loopNo - 1, textView, textViewOut);
                            else
                                animateTexts(actualNo, loopNo + 1, textView, textViewOut);
                        }
                    });
                    animatorSet2.start();
                }
            }, textAnimationFields.getGapBetweenTwoNumbersDuration());
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        textAnimationFields = new TextAnimationFields();

        textAnimationFields.setMaxNumbers(12);
        mGameStatText = findViewById(R.id.game_stat_text);
        linearLayout = findViewById(R.id.linearLayout);
        linearLayout.setVisibility(View.GONE);

        ((Wordio) getApplication()).getAppComponent().inject(this);

        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(GameOverViewModel.class);
        mViewModel.getOnGameDataInfoLoaded().observe(this, this::showGameStat);
        setEditTextFields("1274");
        createNumberOfTextFields();
        if (getIntent().getExtras() != null) {
            mGameId = getIntent().getExtras().getInt(EXTRA_GAME_ROUND_ID);
            mViewModel.loadData(mGameId);
        }

        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Games.SCOPE_GAMES_LITE)

                .build();

        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this,gso);


      GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

        if(account==null){

        }else{
            Games.getLeaderboardsClient(this,account).submitScore(getResources().getString(R.string.leaderboard_id), ActionCenteral.last_score);

        }

    }


    public void onMainMenuClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToMainMenu();
    }

    private void goToMainMenu() {
        if (getPreferences().deleteAfterFinish()) {
            mViewModel.deleteGameRound(mGameId);
        }
        NavUtils.navigateUpTo(this, new Intent());
        finish();
    }

    public void showGameStat(GameDataInfo info) {
        String strGridSize = info.getGridRowCount() + " x " + info.getGridColCount();

        String str = "Finished ";

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Finished");
        stringBuilder.append("\n");
        stringBuilder.append( info.getUsedWordsCount() + " Words ");
        stringBuilder.append("\n");

        stringBuilder.append(  "  Duration " +  DurationFormatter.fromInteger(info.getDuration()) );
        stringBuilder.append("\n");
        stringBuilder.append("  Your Score Is " +  ActionCenteral.last_score );

        mGameStatText.setText(stringBuilder.toString());
    }
}
