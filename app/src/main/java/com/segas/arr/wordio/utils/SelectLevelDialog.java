package com.segas.arr.wordio.utils;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.fragment.app.DialogFragment;

import com.segas.arr.wordio.R;

public class SelectLevelDialog  extends DialogFragment {


    Button easy_btn,medium_btn,hard_btn,veryhard_btn;


    @Override
    public View onCreateView(LayoutInflater inflater,   ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_select_level,container,false);


         easy_btn = view.findViewById(R.id.easy_btn);


    medium_btn = view.findViewById(R.id.medium_btn);


    hard_btn = view.findViewById(R.id.hard_btn);

    veryhard_btn = view.findViewById(R.id.veryhard_btn);


    veryhard_btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SharedPrefrance.SetLevel(3);
            SelectLevelDialog.this.dismiss();

        }
    });

hard_btn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        SharedPrefrance.SetLevel(2);
        SelectLevelDialog.this.dismiss();

    }
});
medium_btn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        SharedPrefrance.SetLevel(1);
        SelectLevelDialog.this.dismiss();

    }
});
    easy_btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SharedPrefrance.SetLevel(0);
            SelectLevelDialog.this.dismiss();
        }
    });





        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);



        return  view;

    }
}
