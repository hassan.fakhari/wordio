package com.segas.arr.wordio.features.gameplay;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.segas.arr.wordio.R;
import com.segas.arr.wordio.features.SoundPlayer;
import com.segas.arr.wordio.features.ViewModelFactory;
import com.segas.arr.wordio.Wordio;
import com.segas.arr.wordio.commons.DurationFormatter;
import com.segas.arr.wordio.commons.Util;
import com.segas.arr.wordio.model.GameData;
import com.segas.arr.wordio.custom.LetterBoard;
import com.segas.arr.wordio.custom.StreakView;
import com.segas.arr.wordio.custom.layout.FlowLayout;
import com.segas.arr.wordio.features.gameover.GameOverActivity;
import com.segas.arr.wordio.features.FullscreenActivity;
import com.segas.arr.wordio.model.UsedWord;
import com.segas.arr.wordio.utils.ActionCenteral;
import com.segas.arr.wordio.utils.SharedPrefrance;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import static com.segas.arr.wordio.utils.ActionCenteral.playAssetSound;


public class GamePlayActivity extends FullscreenActivity {

    public static final String EXTRA_GAME_ROUND_ID =
            "com.aar.app.wordsearch.features.gameplay.GamePlayActivity.ID";
    public static final String EXTRA_ROW_COUNT =
            "com.aar.app.wordsearch.features.gameplay.GamePlayActivity.ROW";
    public static final String EXTRA_COL_COUNT =
            "com.aar.app.wordsearch.features.gameplay.GamePlayActivity.COL";

    private static final StreakLineMapper STREAK_LINE_MAPPER = new StreakLineMapper();
Handler mainHandler;
    @Inject
    SoundPlayer mSoundPlayer;

    @Inject ViewModelFactory mViewModelFactory;
    private GamePlayViewModel mViewModel;

    TextView mTextDuration;
     LetterBoard mLetterBoard;
    FlowLayout mFlowLayout;
TextView score_txt;
      View mTextSelLayout;
     TextView mTextSelection;

     TextView mAnsweredTextCount;
    TextView mWordsCount;


    boolean hint_state = false;

   TextView mFinishedText;
TextView timer_score;
     View mLoading;
      TextView mLoadingText;
      View mContentLayout;
ImageView hint_icon;
     int mGrayColor;
int currentTimer = 0;
    private ArrayLetterGridDataAdapter mLetterAdapter;



    Bundle extras;


    @Override
    public void onBackPressed() {
        ActionCenteral.StopSoundAsset();
        super.onBackPressed();



    }
LinearLayout back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_play);
        playAssetSound(this,"pino.mp3");

mainHandler = new Handler(Looper.getMainLooper());
        timer_score = findViewById(R.id.timer_score);
score_txt = findViewById(R.id.score_txt);
     mTextDuration = findViewById(R.id.text_duration);
        mLetterBoard = findViewById(R.id.letter_board);
        mFlowLayout  = findViewById(R.id.flow_layout);
        mTextSelLayout   = findViewById  (R.id.text_sel_layout);
        mTextSelection    = findViewById(R.id.text_selection);
        mAnsweredTextCount = findViewById((R.id.answered_text_count));
        mWordsCount = findViewById((R.id.words_count));
        mFinishedText = findViewById(R.id.finished_text);
        mLoading = findViewById(R.id.loading);
        mLoadingText = findViewById(R.id.loadingText);
        mContentLayout = findViewById(R.id.content_layout);
        hint_icon = findViewById(R.id.hint_icon);
back = findViewById(R.id.back);






if(SharedPrefrance.GetLevel() == 0){

switch (new Random().nextInt(1) + 1){

    case 1:

        back.setBackgroundResource(R.drawable.pica);

        break;


    case 2:
        back.setBackgroundResource(R.drawable.picb);
        break;

}

}else if(SharedPrefrance.GetLevel() == 1){



    switch (new Random().nextInt(1) + 1){

        case 1:

            back.setBackgroundResource(R.drawable.picc);

            break;


        case 2:
            back.setBackgroundResource(R.drawable.picd);
            break;

    }

}else if(SharedPrefrance.GetLevel() == 2){

    switch (new Random().nextInt(1) + 1){

        case 1:

            back.setBackgroundResource(R.drawable.pice);

            break;


        case 2:
            back.setBackgroundResource(R.drawable.picf);
            break;

    }

}else if(SharedPrefrance.GetLevel() == 3) {


    switch (new Random().nextInt(1) + 1){

        case 1:

            back.setBackgroundResource(R.drawable.picg);

            break;


        case 2:
            back.setBackgroundResource(R.drawable.pich);
            break;

    }
}















        hint_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hint_state == true)
                {

                    return;
                }
             SetHint();
            }
        });

        mGrayColor = getResources().getColor(R.color.gray);


        ((Wordio) getApplication()).getAppComponent().inject(this);

        mLetterBoard.getStreakView().setEnableOverrideStreakLineColor(getPreferences().grayscale());
        mLetterBoard.getStreakView().setOverrideStreakLineColor(mGrayColor);
        mLetterBoard.setOnLetterSelectionListener(new LetterBoard.OnLetterSelectionListener() {
            @Override
            public void onSelectionBegin(StreakView.StreakLine streakLine, String str) {
                streakLine.setColor(Util.getRandomColorWithAlpha(170));
                mTextSelLayout.setVisibility(View.VISIBLE);
                mTextSelection.setText(str);
            }

            @Override
            public void onSelectionDrag(StreakView.StreakLine streakLine, String str) {
                if (str.isEmpty()) {
                    mTextSelection.setText("...");
                } else {
                    mTextSelection.setText(str);
                }
            }

            @Override
            public void onSelectionEnd(StreakView.StreakLine streakLine, String str) {
                mViewModel.answerWord(str, STREAK_LINE_MAPPER.revMap(streakLine), getPreferences().reverseMatching());
                mTextSelLayout.setVisibility(View.INVISIBLE);
                mTextSelection.setText(str);
            }
        });

        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(GamePlayViewModel.class);
        mViewModel.getOnTimer().observe(this, this::showDuration);
        mViewModel.getScore().observe(this,this::onScore);
        mViewModel.getOnGameState().observe(this, this::onGameStateChanged);
        mViewModel.getOnAnswerResult().observe(this, this::onAnswerResult);
mViewModel.getNextLevel().observe(this,this::NewRound);
       extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(EXTRA_GAME_ROUND_ID)) {
                int gid = extras.getInt(EXTRA_GAME_ROUND_ID);
                mViewModel.loadGameRound(gid);
            } else {
                int rowCount = extras.getInt(EXTRA_ROW_COUNT);
                int colCount = extras.getInt(EXTRA_COL_COUNT);
                mViewModel.generateNewGameRound(rowCount, colCount,1,0,mainHandler);
            }
        }

        if (!getPreferences().showGridLine()) {
            mLetterBoard.getGridLineBackground().setVisibility(View.INVISIBLE);
        } else {
            mLetterBoard.getGridLineBackground().setVisibility(View.VISIBLE);
        }

        mLetterBoard.getStreakView().setSnapToGrid(getPreferences().getSnapToGrid());
        mFinishedText.setVisibility(View.GONE);
    }



    int current_round = 1;

    public  void onScore(Integer value){
     score_txt.setText(""+value);
    }
    public void NewRound(Integer value){
        current_round ++ ;


        mFinishedText.setText("Level " + current_round);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mFinishedText.setVisibility(View.GONE);
            }
        },3000);

        if (extras.containsKey(EXTRA_GAME_ROUND_ID)) {
            int gid = extras.getInt(EXTRA_GAME_ROUND_ID);
            mViewModel.loadGameRound(gid);
        } else {
            mLetterBoard.removeAllStreakLine();
mFlowLayout.removeAllViews();
            int rowCount = extras.getInt(EXTRA_ROW_COUNT);
            int colCount = extras.getInt(EXTRA_COL_COUNT);
            mViewModel.generateNewGameRound(rowCount, colCount,current_round,currentTimer,mainHandler);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViewModel.resumeGame();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mViewModel.pauseGame();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.stopGame();
    }


    public  void SetHint(){
        if(getAllUsedWord!=null){

int rnd  = new Random().nextInt(getAllUsedWord.size()) + 0;
UsedWord uw = getAllUsedWord.get(rnd);
TextView tv = findUsedWordTextViewByUsedWordId(uw.getId());

if(uw.isAnswered()==false){
if(mViewModel!=null)
    mViewModel.DecressTimer();


timer_score.setVisibility(View.VISIBLE);
    timer_score.setText("-3");

    Animator anim3 = AnimatorInflater.loadAnimator(this, R.animator.zoom_in_out);
    anim3.setTarget(timer_score);
    anim3.start();

hint_state = true;
    new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {

            hint_state = false;
            timer_score.setVisibility(View.GONE);
        }
    },3000);


        int revealCount = uw.getmString().length();//uw.getRevealCount();
        String uwString = uw.getString();
        String str = "";
        for (int i = 0; i < uwString.length(); i++) {
            if (revealCount > 0) {
                str += uwString.charAt(i);
                revealCount--;
            }
            else {
                str += " -";
            }
        }

    tv.setText(str);
    Animator anim = AnimatorInflater.loadAnimator(this, R.animator.zoom_in_out);
    anim.setTarget(tv);
    anim.start();
    mSoundPlayer.play(SoundPlayer.Sound.Correct);

}
        }
    }

    private void onAnswerResult(GamePlayViewModel.AnswerResult answerResult) {
        if (answerResult.correct) {
            TextView textView = findUsedWordTextViewByUsedWordId(answerResult.usedWordId);
            if (textView != null) {
                UsedWord uw = (UsedWord) textView.getTag();

                if (getPreferences().grayscale()) {
                    uw.getAnswerLine().color = mGrayColor;
                }
                textView.setBackgroundColor(uw.getAnswerLine().color);
                textView.setText(uw.getString());
                textView.setTextColor(Color.WHITE);
                textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                Animator anim = AnimatorInflater.loadAnimator(this, R.animator.zoom_in_out);
                anim.setTarget(textView);
                anim.start();
            }

            mSoundPlayer.play(SoundPlayer.Sound.Correct);
        }
        else {
            mLetterBoard.popStreakLine();

            mSoundPlayer.play(SoundPlayer.Sound.Wrong);
        }
    }

    private void onGameStateChanged(GamePlayViewModel.GameState gameState) {
        showLoading(false, null);
        if (gameState instanceof GamePlayViewModel.Generating) {
            GamePlayViewModel.Generating state = (GamePlayViewModel.Generating) gameState;
            String text = "Generating " + state.rowCount + "x" + state.colCount + " grid";
            showLoading(true, text);
        } else if (gameState instanceof GamePlayViewModel.Finished) {
            showFinishGame(((GamePlayViewModel.Finished) gameState).mGameData.getId());
        } else if (gameState instanceof GamePlayViewModel.Paused) {

        } else if (gameState instanceof GamePlayViewModel.Playing) {
            onGameRoundLoaded(((GamePlayViewModel.Playing) gameState).mGameData);
        }else if(gameState instanceof GamePlayViewModel.NextLevel){
            GamePlayViewModel.NextLevel state = (GamePlayViewModel.NextLevel) gameState;
            String text = "Generating " + state.rowCount + "x" + state.colCount + " grid";
            //showLoading(false, text);
            //onGameRoundLoaded(((GamePlayViewModel.NextLevel) gameState).mGameData);
        }
    }
List<UsedWord> getAllUsedWord;




    private void onGameRoundLoaded(GameData gameData) {
        if (gameData.isFinished()) {
    //        setGameAsAlreadyFinished();
        }

        showLetterGrid(gameData.getGrid().getArray());
        showDuration(gameData.getDuration());

        showUsedWords(gameData.getUsedWords());

     getAllUsedWord = gameData.getUsedWords();

        showWordsCount(gameData.getUsedWords().size());
        showAnsweredWordsCount(gameData.getAnsweredWordsCount());
        doneLoadingContent();
    }

    private void tryScale() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int boardWidth = mLetterBoard.getWidth();
        int screenWidth = metrics.widthPixels;

        if (getPreferences().autoScaleGrid() || boardWidth > screenWidth) {
            float scale = (float)screenWidth / (float)boardWidth;
            mLetterBoard.scale(scale, scale);
           /*
            mLetterBoard.animate()
                    .scaleX(scale)
                    .scaleY(scale)
                    .setDuration(400)
                    .setInterpolator(new DecelerateInterpolator())
                    .start();
                    */
        }
    }

    private void doneLoadingContent() {
        // call tryScale() on the next render frame
        new Handler().postDelayed(this::tryScale, 100);
    }

    private void showLoading(boolean enable, String text) {
        if (enable) {
            mLoading.setVisibility(View.VISIBLE);
            mLoadingText.setVisibility(View.VISIBLE);
            mContentLayout.setVisibility(View.GONE);
            mLoadingText.setText(text);
        } else {
            mLoading.setVisibility(View.GONE);
            mLoadingText.setVisibility(View.GONE);
            mContentLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showLetterGrid(char[][] grid) {
        if (mLetterAdapter == null) {
            mLetterAdapter = new ArrayLetterGridDataAdapter(grid);
            mLetterBoard.setDataAdapter(mLetterAdapter);
        }
        else {
            mLetterAdapter.setGrid(grid);
        }
    }

    private void showDuration(int duration) {
        this.currentTimer = duration;
        if(current_round >= 1){
            this.currentTimer = this.currentTimer + 20;
        }
        mTextDuration.setText(DurationFormatter.fromInteger(duration));
    }

    private void showUsedWords(List<UsedWord> usedWords) {
        for (UsedWord uw : usedWords) {
            mFlowLayout.addView( createUsedWordTextView(uw) );

        }
    }

    private void showAnsweredWordsCount(int count) {
        mAnsweredTextCount.setText(String.valueOf(count));
    }

    private void showWordsCount(int count) {
        mWordsCount.setText(String.valueOf(count));
    }

    private void showFinishGame(int gameId) {
        Intent intent = new Intent(this, GameOverActivity.class);
        intent.putExtra(GameOverActivity.EXTRA_GAME_ROUND_ID, gameId);
        ActionCenteral.StopSoundAsset();
        startActivity(intent);
        finish();
    }

    private void setGameAsAlreadyFinished() {
        mLetterBoard.getStreakView().setInteractive(false);
        mFinishedText.setVisibility(View.VISIBLE);
    }

    //
    private TextView createUsedWordTextView(UsedWord uw) {
        TextView tv = new TextView(this);
        tv.setPadding(10, 5, 10, 5);
tv.setTextColor(getResources().getColor(R.color.white));
tv.setTextSize(16);
        if (uw.isAnswered()) {
            if (getPreferences().grayscale()) {
                uw.getAnswerLine().color = mGrayColor;
            }

            tv.setBackgroundColor(uw.getAnswerLine().color);
            tv.setText(uw.getString());
            tv.setTextColor(Color.WHITE);
            tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mLetterBoard.addStreakLine(STREAK_LINE_MAPPER.map(uw.getAnswerLine()));
        }
        else {
            String str = uw.getString();
            if (uw.isMystery()) {
                int revealCount = 2;//uw.getRevealCount();
                String uwString = uw.getString();
                str = "";
                for (int i = 0; i < uwString.length(); i++) {
                    if (revealCount > 0) {
                        str += uwString.charAt(i);
                        revealCount--;
                    }
                    else {
                        str += " -";
                    }
                }
            }
            tv.setText(str);
        }

        tv.setTag(uw);
        return tv;
    }

    private TextView findUsedWordTextViewByUsedWordId(int usedWordId) {
        for (int i = 0; i < mFlowLayout.getChildCount(); i++) {
            TextView tv = (TextView) mFlowLayout.getChildAt(i);
            UsedWord uw = (UsedWord) tv.getTag();
            if (uw != null && uw.getId() == usedWordId) {
                return tv;
            }
        }

        return null;
    }
}
