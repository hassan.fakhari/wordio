package com.segas.arr.wordio.features.gameplay;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.segas.arr.wordio.commons.SingleLiveEvent;
import com.segas.arr.wordio.commons.Timer;
import com.segas.arr.wordio.data.GameDataSource;
import com.segas.arr.wordio.data.entity.GameDataMapper;
import com.segas.arr.wordio.data.WordDataSource;
import com.segas.arr.wordio.model.GameData;
import com.segas.arr.wordio.model.UsedWord;
import com.segas.arr.wordio.model.Word;
import com.segas.arr.wordio.utils.ActionCenteral;

import java.util.List;
import java.util.logging.Handler;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class GamePlayViewModel extends ViewModel {

    private static final int TIMER_TIMEOUT = 1000;

    static abstract class GameState {}
    static class Generating extends GameState {
        int rowCount;
        int colCount;
        String name;
        private Generating(int rowCount, int colCount, String name) {
            this.rowCount = rowCount;
            this.colCount = colCount;
            this.name = name;
        }
    }




    static class NextLevel extends  GameState{

        int rowCount;
        int colCount;
        String name;
        private NextLevel(int rowCount, int colCount, String name) {
            this.rowCount = rowCount;
            this.colCount = colCount;
            this.name = name;
        }



    }



    static class Loading extends GameState {
        int gid;
        private Loading(int gid) {
            this.gid = gid;
        }
    }
    static class Finished extends GameState {
        GameData mGameData;
        private Finished(GameData gameData) {
            this.mGameData = gameData;
        }
    }
    static class Paused extends GameState {
        private Paused() {}
    }
    static class Playing extends GameState {
        GameData mGameData;
        private Playing(GameData gameData) {
            this.mGameData = gameData;
        }
    }

    static class AnswerResult {
        public boolean correct;
        public int usedWordId;
        AnswerResult(boolean correct, int usedWordId) {
            this.correct = correct;
            this.usedWordId = usedWordId;
        }
    }

    private GameDataSource mGameDataSource;
    private WordDataSource mWordDataSource;
    private GameDataCreator mGameDataCreator;
    private GameData mCurrentGameData;
    private Timer mTimer;
    private int mCurrentDuration;

    private GameState mCurrentState = null;
    private MutableLiveData<Integer> mOnTimer;
    private MutableLiveData<GameState> mOnGameState;
    private SingleLiveEvent<AnswerResult> mOnAnswerResult;
private  SingleLiveEvent<Integer> onNextRound;
private SingleLiveEvent<Integer> onScore;
    public GamePlayViewModel(GameDataSource gameDataSource, WordDataSource wordDataSource) {
        mGameDataSource = gameDataSource;
        mWordDataSource = wordDataSource;
        mGameDataCreator = new GameDataCreator();

        mTimer = new Timer(TIMER_TIMEOUT);
        mTimer.addOnTimeoutListener(elapsedTime -> {
            mOnTimer.setValue(mCurrentDuration--);

            if(mCurrentDuration <=0){
               // generateNewGameRound(6,6);
              setGameState(new Finished(mCurrentGameData));
                return;

            }

            mGameDataSource.saveGameDataDuration(mCurrentGameData.getId(), mCurrentDuration);
        });




                resetLiveData();
    }








    private void resetLiveData() {

            mOnTimer = new MutableLiveData<>();
            mOnGameState = new MutableLiveData<>();
            mOnAnswerResult = new SingleLiveEvent<>();
onNextRound = new SingleLiveEvent<>();
onScore = new SingleLiveEvent<>();
    }

    public void stopGame() {
        mCurrentGameData = null;
        mTimer.stop();
        resetLiveData();
    }

    public void pauseGame() {
        mTimer.stop();
        setGameState(new Paused());
    }

    public void resumeGame() {
        if (mCurrentState instanceof Paused) {
            mTimer.start();
            setGameState(new Playing(mCurrentGameData));
        }
    }

    public void loadGameRound(int gid) {
        if (!(mCurrentState instanceof Generating)) {
            setGameState(new Loading(gid));

            mGameDataSource.getGameData(gid, gameRound -> {
                mCurrentGameData = new GameDataMapper().map(gameRound);
                mCurrentDuration = mCurrentGameData.getDuration();
                if (!mCurrentGameData.isFinished())
                    mTimer.start();
                setGameState(new Playing(mCurrentGameData));
            });
        }
    }
int currentRow,correntCol;
    int current_timer = 0 ;
    @SuppressLint("CheckResult")
    public void generateNewGameRound(int rowCount, int colCount, int level, int current_timer, android.os.Handler mainHandler) {
        if (!(mCurrentState instanceof Generating)) {
            setGameState(new Generating(rowCount, colCount, "Play me"));



            if(current_timer == 0){
this.current_timer = 50;
            }else{
               this.current_timer = current_timer;
            }


            Observable.create((ObservableOnSubscribe<GameData>) emitter -> {
                mWordDataSource.InsertWords();
                List<Word> wordList = mWordDataSource.getWords(level);
                if(wordList.size() <=0){
mainHandler.post(new Runnable() {
    @Override
    public void run() {
        setGameState(new Finished(mCurrentGameData));

    }
});

                    return;
                }


                currentRow = rowCount;
                correntCol = colCount;
                GameData gr = mGameDataCreator.newGameData(wordList, rowCount, colCount, "Play me");
                long gid = mGameDataSource.saveGameData(new GameDataMapper().revMap(gr));
                gr.setId((int) gid);
                emitter.onNext(gr);
                emitter.onComplete();
            }).subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gameRound -> {




                        mCurrentDuration = this.current_timer;

                        mTimer.start();
                        mCurrentGameData = gameRound;

                        setGameState(new Playing(mCurrentGameData));
                    });

            }

    }




public  void DecressTimer(){
    mCurrentDuration = mCurrentDuration - 3;
}



    public void answerWord(String answerStr, UsedWord.AnswerLine answerLine, boolean reverseMatching) {
        UsedWord correctWord = mCurrentGameData.markWordAsAnswered(answerStr, answerLine, reverseMatching);

        boolean correct = correctWord != null;

        mOnAnswerResult.setValue(new AnswerResult(correct, correctWord != null ? correctWord.getId() : -1));
        if (correct) {
            mGameDataSource.markWordAsAnswered(correctWord);

if(correctWord.getLevel() == 1) {
    mCurrentDuration = mCurrentDuration + 3;
}else if(correctWord.getLevel() == 2){
    mCurrentDuration = mCurrentDuration + 5;
}else if(correctWord.getLevel() == 3){
    mCurrentDuration = mCurrentDuration + 10;
}




int word_count = correctWord.getmString().length();

if(word_count==correctWord.getRevealCount()) {
    ActionCenteral.last_score = ActionCenteral.last_score + 2;
}else if(correctWord.getRevealCount() == (word_count / 2)){

    ActionCenteral.last_score = ActionCenteral.last_score + 4;

}else {
    ActionCenteral.last_score = ActionCenteral.last_score + 5;

}

onScore.setValue(ActionCenteral.last_score);

            if (mCurrentGameData.isFinished()) {
              // setGameState(new Generating(currentRow,correntCol,"Play me"));



onNextRound.setValue(1);



             }
        }
    }




public  LiveData<Integer> getNextLevel(){ return  onNextRound; }
    public LiveData<Integer> getOnTimer() {
        return mOnTimer;
    }

    public LiveData<GameState> getOnGameState() {
        return mOnGameState;
    }
public LiveData<Integer> getScore(){return onScore;}
    public LiveData<AnswerResult> getOnAnswerResult() {
        return mOnAnswerResult;
    }

    private void setGameState(GameState state) {
        mCurrentState = state;
        mOnGameState.setValue(mCurrentState);
    }
}
