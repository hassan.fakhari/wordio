package com.segas.arr.wordio.data;

import com.segas.arr.wordio.model.Word;

import java.util.List;



public interface WordDataSource {

    List<Word> getWords(int level);


    void InsertWords();



}
