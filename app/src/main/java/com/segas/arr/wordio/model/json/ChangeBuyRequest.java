package com.segas.arr.wordio.model.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

public class ChangeBuyRequest {


    @Expose
    @SerializedName("method")
    private String method;


    @Nullable
    @SerializedName("device_id")
    private String device_id;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}
