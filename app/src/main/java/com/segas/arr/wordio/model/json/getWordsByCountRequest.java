package com.segas.arr.wordio.model.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class getWordsByCountRequest {



    @Expose
    @SerializedName("count")
    private  String count;


    @Expose
    @SerializedName("method")
    private  String method;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
