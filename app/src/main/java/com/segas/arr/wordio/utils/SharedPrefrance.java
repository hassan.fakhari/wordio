package com.segas.arr.wordio.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.segas.arr.wordio.R;

public class SharedPrefrance {


    static SharedPreferences preferences;
    static  SharedPreferences.Editor editor;
    private static String KEY_ENABLE_SOUND;



    public  SharedPrefrance(Context con){
        preferences = con.getSharedPreferences("word",Context.MODE_PRIVATE);
        editor = preferences.edit();

        KEY_ENABLE_SOUND = con.getString(R.string.pref_enableSound);
    }
public  static  void SetSound(boolean state){
        editor.putBoolean(KEY_ENABLE_SOUND,state);
        editor.commit();
}
public  static  boolean GetSound(){
        return  preferences.getBoolean(KEY_ENABLE_SOUND,false);
}
public static void SetTable(int i){
        editor.putInt("table",i);
        editor.commit();
}
public  static  int GetTable(){
        return  preferences.getInt("table",0);

}

public  static  void SetLevel(int i){
        editor.putInt("level",i);
        editor.commit();
}
public  static  int GetLevel(){
        return  preferences.getInt("level",0);

}















}
