package com.segas.arr.wordio.di.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.segas.arr.wordio.features.ViewModelFactory;
import com.segas.arr.wordio.data.GameDataSource;
import com.segas.arr.wordio.data.GameThemeRepository;
import com.segas.arr.wordio.data.WordDataSource;
import com.segas.arr.wordio.features.gamehistory.GameHistoryViewModel;
import com.segas.arr.wordio.features.gameover.GameOverViewModel;
import com.segas.arr.wordio.features.gameplay.GamePlayViewModel;
import com.segas.arr.wordio.features.mainmenu.MainMenuViewModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;



@Module
public class AppModule {

    private Application mApp;

    public AppModule(Application application) {
        mApp = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApp;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    ViewModelFactory provideViewModelFactory(GameDataSource gameDataSource,
                                             WordDataSource wordDataSource) {
        return new ViewModelFactory(
                new GameOverViewModel(gameDataSource),
                new GamePlayViewModel(gameDataSource, wordDataSource),
                new MainMenuViewModel(new GameThemeRepository()),
                new GameHistoryViewModel(gameDataSource)
        );
    }
}
