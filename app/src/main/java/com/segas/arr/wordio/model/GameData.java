package com.segas.arr.wordio.model;

import com.segas.arr.wordio.commons.Util;

import java.util.ArrayList;
import java.util.List;


public class GameData {

    private int mId;
    private String mName;
    private int mDuration;
    private Grid mGrid;
    private List<UsedWord> mUsedWords;
private  int level;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmDuration() {
        return mDuration;
    }

    public void setmDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public Grid getmGrid() {
        return mGrid;
    }

    public void setmGrid(Grid mGrid) {
        this.mGrid = mGrid;
    }

    public List<UsedWord> getmUsedWords() {
        return mUsedWords;
    }

    public void setmUsedWords(List<UsedWord> mUsedWords) {
        this.mUsedWords = mUsedWords;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public GameData() {
        this(0, "", 0, null, new ArrayList<>(),0);
    }

    public GameData(int id, String name, int duration, Grid grid, List<UsedWord> usedWords,int level) {
        mId = id;
        this.level = level;
        mName = name;
        mDuration = duration;
        mGrid = grid;
        mUsedWords = usedWords;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public Grid getGrid() {
        return mGrid;
    }

    public void setGrid(Grid grid) {
        mGrid = grid;
    }

    public List<UsedWord> getUsedWords() {
        return mUsedWords;
    }

    public UsedWord markWordAsAnswered(String word, UsedWord.AnswerLine answerLine, boolean enableReverse) {
        String answerStrRev = Util.getReverseString(word);
        for (UsedWord usedWord : mUsedWords) {

            if (usedWord.isAnswered()) continue;

            String currUsedWord = usedWord.getString();
            if (currUsedWord.equalsIgnoreCase(word) ||
                    (currUsedWord.equalsIgnoreCase( answerStrRev ) && enableReverse)) {

                usedWord.setAnswered(true);
                usedWord.setAnswerLine(answerLine);
                return usedWord;
            }
        }
        return null;
    }

    public int getAnsweredWordsCount() {
        int count = 0;
        for (UsedWord uw : mUsedWords) {
            if (uw.isAnswered()) count++;
        }
        return count;
    }

    public boolean isFinished() {
        return getAnsweredWordsCount() == mUsedWords.size();
    }

    public void addUsedWord(UsedWord usedWord) {
        mUsedWords.add(usedWord);
    }

    public void addUsedWords(List<UsedWord> usedWords) {
        mUsedWords.addAll(usedWords);
    }
}
