package com.segas.arr.wordio.model.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.reactivex.annotations.Nullable;

public class LoginUserRequest {






    //LoginUser


    @Nullable
    @Expose
    @SerializedName("user_name")
    private  String user_name;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @Expose
    @SerializedName("method")
    private String method;


    @Nullable
    @SerializedName("device_id")
    private String device_id;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    @Nullable
    @Expose
    @SerializedName("phone_number")
    private  String phone_number;



}
