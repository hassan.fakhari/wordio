package com.segas.arr.wordio.features.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.segas.arr.wordio.R;
import com.segas.arr.wordio.utils.SharedPrefrance;



public class SettingsActivity extends AppCompatActivity {


    CheckBox sound_ms;
Button save_btn;
     RadioButton pr_rad,en_rad;
    private SharedPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.setting_activity);
        sound_ms = findViewById(R.id.sound_ms);


        sound_ms.setChecked(SharedPrefrance.GetSound());
        pr_rad = findViewById(R.id.pr_rad);
        en_rad = findViewById(R.id.en_rad);
        save_btn = findViewById(R.id.save_btn);


        if(SharedPrefrance.GetTable()==0){
            en_rad.setChecked(true);
            pr_rad.setChecked(false);
        }else if(SharedPrefrance.GetTable() == 1){
            en_rad.setChecked(false);
            pr_rad.setChecked(true);
        }

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(en_rad.isChecked()==true){
                    SharedPrefrance.SetTable(0);
                }else if(pr_rad.isChecked() == true){
                    SharedPrefrance.SetTable(1);
                }

                SharedPrefrance.SetSound(sound_ms.isChecked());

                Toast.makeText(SettingsActivity.this,"Saved!",Toast.LENGTH_LONG).show();

            }
        });







    }

}
