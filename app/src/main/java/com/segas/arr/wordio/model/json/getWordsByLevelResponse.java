package com.segas.arr.wordio.model.json;

import com.segas.arr.wordio.model.OnlineWord;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class getWordsByLevelResponse {

    @Expose
    @SerializedName("message")
    private String message;


    @Expose
    @SerializedName("data")
    private List<OnlineWord> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OnlineWord> getData() {
        return data;
    }

    public void setData(List<OnlineWord> data) {
        this.data = data;
    }
}
